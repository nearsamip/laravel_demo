<?php

namespace App\Library;
use Excel;
use Image;
use SimpleSoftwareIO\QrCode\Facades\QrCode;
use File;



class FileLibrary {

    static function getAllData($file)
    {
         // Get the csv rows as an array
        // $theArray = Excel::toArray(new stdClass(), $request->file('csv_file'));

        // Get the csv rows as a collection
        // $theCollection = Excel::toCollection(collect([]), $request->file('csv_file'));

        $data['fileData']  = Excel::toArray(array([]), $file );

        // echo '<pre>';

        // print_r($data['fileData']);die;
        
        $count = count( $data['fileData']);
        if ( $count ) {
            
            return $data['fileData'];
            
        }else{
            return null;
        }
        return null;
    }


    static  function fileUpload($newFile,$path,$user_id,$oldFile=NULL,$width=NULL,$height=NULL)
    {

        if ($newFile ) {
            $extension = $newFile->getClientOriginalExtension();

            $fileName = $user_id."_".Date('Y-m-d')."_".rand()."_".time().'.'.$extension;

            $newFile->move(public_path($path), $fileName);

            /*if old file exits then delete the file*/
            if ($oldFile) {
                if ( file_exists(public_path($path."/".$oldFile)) ) {
                    unlink(public_path($path."/".$oldFile));
                }
                
                
                
            }
            return $fileName;
            
        }else {

            return $oldFile;
        }
        
        
    }

    static function generateImageFromQrcodes($qrcode)
    {
        $image = \QrCode::format('png')
                ->size(200)
                 ->generate($qrcode);
       
        
        $fileName = "ticket_".time().rand(000000000,999999999)."png";
        File::put('uploads/'.$fileName, $image);
        return $fileName;
    }



    // static function getFileExtension( $file )
    // {
    //     $extension = $file->extension();
    // }

    static  function contact_import_file_validation($reader)
    {
        $headerRow = $reader->first()->keys()->toArray();
        if ( in_array("first_name", $headerRow) &&  ( in_array("last_name", $headerRow)  && in_array("mobile", $headerRow) ) )
        {
            return 1;
        }
        else
        {
            return NULL;
        }
    }


    

    


    

    


}