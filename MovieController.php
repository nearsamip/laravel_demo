<?php

namespace App\Http\Controllers\Api;

use App\Services\MovieService;
use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Http\Requests\MovieRequest;
use Hash;
use Auth;
use App\Library\ApiResponse;



class MovieController extends Controller
{
    /**
     *
     * @var $log
     * @var $movieService
     */


    protected $movieService;

    public function __construct(
        MovieService $movieService
    ){
        $this->movieService = $movieService;
    }

    

    public function list( MovieRequest $request )
    {
        $data = $request->validated();
        $response = $this->movieService->list($data);
        // print_r($response['data']);die;
        return ApiResponse::sendResponse($response['error'],$response['message'],$response['data'],200);
    }


    public function movieListForTicketCounter( MovieRequest $request )
    {
        $data = $request->validated();
        $response = $this->movieService->movieListForTicketCounter($data);
        // print_r($response['data']);die;
        return ApiResponse::sendResponse($response['error'],$response['message'],$response['data'],200);
    }



    public function listUpcomming( MovieRequest $request )
    {
        $data = $request->validated();
        $response = $this->movieService->listUpcomming($data);
        // print_r($response['data']);die;
        return ApiResponse::sendResponse($response['error'],$response['message'],$response['data'],200);
    }


    

    public function store( MovieRequest $request )
    {
         $data = $request->validated();
        $response = $this->movieService->store($data);
        return ApiResponse::sendResponse($response['error'],$response['message'],$response['data'],200);

    }


     public function clone( MovieRequest $request )
    {
         $data = $request->validated();
        $response = $this->movieService->clone($data);
        return ApiResponse::sendResponse($response['error'],$response['message'],$response['data'],200);

    }

   

    public function update( MovieRequest $request )
    {
         $data = $request->validated();
        $response = $this->movieService->update($data);
        return ApiResponse::sendResponse($response['error'],$response['message'],$response['data'],200);
    }

    public function delete( MovieRequest $request )
    {
         $data = $request->validated();
        $response = $this->movieService->delete($data);
        return ApiResponse::sendResponse($response['error'],$response['message'],$response['data'],200);
    }


    public function storeUpcomming( MovieRequest $request )
    {
         $data = $request->validated();
        $response = $this->movieService->storeUpcomming($data);
        return ApiResponse::sendResponse($response['error'],$response['message'],$response['data'],200);

    }

   

    public function updateUpcomming( MovieRequest $request )
    {
         $data = $request->validated();
        $response = $this->movieService->updateUpcomming($data);
        return ApiResponse::sendResponse($response['error'],$response['message'],$response['data'],200);
    }

    public function deleteUpcomming( MovieRequest $request )
    {
         $data = $request->validated();
        $response = $this->movieService->deleteUpcomming($data);
        return ApiResponse::sendResponse($response['error'],$response['message'],$response['data'],200);
    }

    
}
